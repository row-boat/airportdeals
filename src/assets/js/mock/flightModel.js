define(
	['jquery', 'ko'],
	function($, ko){
		var ctor = function() {
			var self = this;

			self.price = ko.observable(0);
			self.destination = ko.observable();
			self.airline = ko.observable();
			self.classType = ko.observable();
			self.numberOfStops = ko.observable();			

		};
		return ctor;
});