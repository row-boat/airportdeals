define(
	['jquery', 'ko', 'mock/flightModel'],
	function($, ko, FlightModel) {

		return {

			flights : ko.observableArray([]),

			moqData : function() {
				var prices = [680, 700, 890, 1020, 230];
				var destinations = [
					'LAX',
					'CEB',
					'MNL',
					'HKG',
					'IAH'
					];
				var airlines = [
					'American Airlines',
					'Virgin America',
					'United',
					'Malaysian Airlines',
					'Emirates'
					];
				var classes = [
					'Economy',
					'Business',
					'First',
					'Premium Economy',
					'Super Economy'
					];
				var numberOfStops = [0, 1,2,3,4];

				var totalRecords = 30;
				var idx = 0;
				var options = 4;
				for(var i = 0; i < totalRecords; i++) {
					var flight = new FlightModel();
					
					flight.price(prices[idx]);
					flight.destination(destinations[idx]);
					flight.airline(airlines[idx]);
					flight.classType(classes[idx]);
					flight.numberOfStops(numberOfStops[idx]);
					this.flights.push(flight);

					idx = (idx + 1) > options ? 0 : (idx + 1);
				}

			}

		}

	}
);