requirejs.config({
	//!!NOTE!! don't forget to change this to v# (#=as a number) before rolling out to dev or production
	urlArgs: "v=" + (new Date()).getTime(), 
	baseUrl: 'assets/js',
	paths: {
		'jquery' : [
			'//code.jquery.com/jquery-1.11.0.min',
			'lib/jquery-1.11.0.min'			
		],
		'bootstrap': [
			'//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min',
			'lib/bootstrap.min'
		],
		'bootstrap.datetimepicker': 'lib/bootstrap-datetimepicker.min',
		'moment' : 'lib/moment-with-langs.min',
		'ko': 'lib/knockout-3.0.min',
		'jquery.tablesorter' : 'lib/jquery.tablesorter.combined',
		'typeahead' : 'lib/typeahead.bundle'
	},

	shim : {
		'bootstrap' : {
			deps: ['jquery']
		},
		'bootstrap.datetimepicker' : {
			deps: ['jquery', 'bootstrap', 'moment']
		},
		'jquery.tablesorter' : {
			deps: ['jquery']
		},
		'typeahead' : { 
			deps: ['jquery']
		}

	}
});

//activate bootstrap by default
require(['bootstrap']);